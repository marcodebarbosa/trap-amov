package marcobarbosa21200304.trap.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import marcobarbosa21200304.trap.R;

public class SplashActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    // TODO: Change the slash time
    private static final int SPLASH_TIME_OUT = 1000;

    @Bind(R.id.tv_splash_title)
    TextView tvTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/riverdrive.ttf");

        tvTitle.setTypeface(typeFace);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
