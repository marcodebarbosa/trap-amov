package marcobarbosa21200304.trap.Activities;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import marcobarbosa21200304.trap.R;
import marcobarbosa21200304.trap.util.Constants;

public class BestScoreActivity extends AppCompatActivity {

    @Bind(R.id.tv_best_score)
    TextView tvBestScoreTitle;
    @Bind(R.id.tv_best_score_name1)
    TextView tvBestScoreName1;
    @Bind(R.id.tv_best_score_points1)
    TextView tvBestScorePoints1;
    @Bind(R.id.tv_best_score_name2)
    TextView tvBestScoreName2;
    @Bind(R.id.tv_best_score_points2)
    TextView tvBestScorePoints2;
    @Bind(R.id.tv_best_score_name3)
    TextView tvBestScoreName3;
    @Bind(R.id.tv_best_score_points3)
    TextView tvBestScorePoints3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_best_score);

        ButterKnife.bind(this);

        SharedPreferences sharedpreferences = getSharedPreferences(Constants.MY_PREFERENCES, MODE_PRIVATE);

        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/riverdrive.ttf");
        tvBestScoreTitle.setTypeface(typeFace);

        tvBestScoreName1.setShadowLayer(1, 0, 0, Color.BLACK);
        tvBestScorePoints1.setShadowLayer(1, 0, 0, Color.BLACK);
        tvBestScoreName2.setShadowLayer(1, 0, 0, Color.BLACK);
        tvBestScorePoints2.setShadowLayer(1, 0, 0, Color.BLACK);
        tvBestScoreName3.setShadowLayer(1, 0, 0, Color.BLACK);
        tvBestScorePoints3.setShadowLayer(1, 0, 0, Color.BLACK);

        tvBestScoreName1.setText(sharedpreferences.getString("playerName1", "Empty"));
        tvBestScorePoints1.setText(String.valueOf(sharedpreferences.getInt("playerBestScore1", 0)));

        tvBestScoreName2.setText(sharedpreferences.getString("playerName2", "Empty"));
        tvBestScorePoints2.setText(String.valueOf(sharedpreferences.getInt("playerBestScore2", 0)));

        tvBestScoreName3.setText(sharedpreferences.getString("playerName3", "Empty"));
        tvBestScorePoints3.setText(String.valueOf(sharedpreferences.getInt("playerBestScore3", 0)));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
