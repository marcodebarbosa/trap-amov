package marcobarbosa21200304.trap.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import marcobarbosa21200304.trap.Application.GlobalStateApplication;
import marcobarbosa21200304.trap.Communication.Communication;
import marcobarbosa21200304.trap.Player.Player;
import marcobarbosa21200304.trap.R;
import marcobarbosa21200304.trap.Views.GameView;
import marcobarbosa21200304.trap.util.Constants;

public class GameBoardActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    // General Variables
    private GlobalStateApplication global;
    private Handler handler = null;
    private ProgressDialog pd;
    private GameBoardBR receiver;
    private SurfaceView gameView;

    // Communication variables
    private ServerSocket serverSocket = null;
    private Socket socketGame = null;

    @Bind(R.id.ll_game_area)
    LinearLayout container;
    @Bind(R.id.btn_create_game)
    Button btnCreateGame;
    @Bind(R.id.btn_join_game)
    Button btnJoinGame;
    @Bind(R.id.tv_multi_player_title)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_board);

        global = (GlobalStateApplication) getApplicationContext();

        ButterKnife.bind(this);

        if (!getIntent().getBooleanExtra("isMultiPlayer", false)) {
            try {
                registerBroadcastReceiver();
                handleSinglePlayer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            tvTitle.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/riverdrive.ttf"));
            handler = new Handler();
            handleMultiplayer();
            registerBroadcastReceiver();
        }
    }

    private void handleSinglePlayer() throws IOException {

        SurfaceView gameView = new GameView(this, global, 4, Constants.BLACK, Constants.GREEN, Constants.BLUE);
        container.addView(gameView);
    }

    private void handleMultiplayer() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tvTitle.setVisibility(View.VISIBLE);
        btnCreateGame.setVisibility(View.VISIBLE);
        btnJoinGame.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_create_game)
    void createGame() {

        if (isConnectedToNetwork()) {
            String ip = getLocalIpAddress();
            pd = new ProgressDialog(this);
            pd.setMessage(global.getResources().getString(R.string.waiting_for_the_opponent) + "\n(IP:" + ip + ")");
            pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    //finish();
                    if (serverSocket != null) {
                        try {
                            serverSocket.close();
                        } catch (IOException e) {
                        }
                        serverSocket = null;
                    }
                }
            });
            pd.show();

            server();
        }
    }

    @OnClick(R.id.btn_join_game)
    void joinGame() {

        if (isConnectedToNetwork()) {
            final EditText edtIP = new EditText(this);
            edtIP.setText("192.168.208.102");
            AlertDialog ad = new AlertDialog.Builder(this)
                    .setMessage(global.getResources().getString(R.string.set_ip_server))
                    .setView(edtIP)
                    .setPositiveButton(global.getResources().getString(R.string.confirm),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog, int which) {
                                    client(edtIP.getText().toString(), Constants.PORTaux);
                                }
                            })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(
                                DialogInterface dialog) {
//                        finish();
                        }
                    })
                    .create();
            ad.show();
        }
    }

    private void server() {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serverSocket = new ServerSocket(Constants.PORT);
                    socketGame = serverSocket.accept();
                    serverSocket.close();
                    serverSocket = null;

                } catch (Exception e) {
                    socketGame = null;
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        pd.dismiss();

                        if (socketGame == null) {
                            Toast.makeText(getApplicationContext(), global.getResources().getString(R.string.socket_turned_off), Toast.LENGTH_LONG).show();
                        } else {

                            // Screen modifications
                            tvTitle.setVisibility(View.GONE);
                            btnCreateGame.setVisibility(View.GONE);
                            btnJoinGame.setVisibility(View.GONE);
                            pd = ProgressDialog.show(GameBoardActivity.this, "", global.getResources().getString(R.string.preparing_the_game), true);
                            pd.setIndeterminate(true);

                            // Player variables
                            global.getPlayer().setSocket(socketGame);
                            global.getPlayer().setIsServer(true);
                            global.setPlayerOpponent(new Player(false, 1));
                            global.getPlayer().setIsMyTurn(true);

                            global.communication = new Communication(global, GameBoardActivity.this);

                            gameView = new GameView(GameBoardActivity.this, global);
//                            container.addView(gameView);
                        }
                    }
                });
            }
        });
        t.start();
    }

    private void client(final String strIP, final int Port) {

        Thread t = new Thread
                (new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.i(TAG, "Vou ligar ao servidor " + strIP);
                            socketGame = new Socket(strIP, Constants.PORT);
                        } catch (Exception e) {
                            socketGame = null;
                        }
                        if (socketGame == null) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            });
                        } else {
                            GameBoardActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    // Setting player stuff

                                    global.getPlayer().setIsServer(false);
                                    global.getPlayer().setSocket(socketGame);
                                    global.setPlayerOpponent(new Player(true, 1));
                                    global.getPlayer().setIsMyTurn(false);

                                    // Progress Dialog waiting for server's first message
                                    pd = ProgressDialog.show(GameBoardActivity.this, "", global.getResources().getString(R.string.preparing_the_game), true);
                                    pd.setIndeterminate(true);

                                    //Screen changes
                                    tvTitle.setVisibility(View.GONE);
                                    btnCreateGame.setVisibility(View.GONE);
                                    btnJoinGame.setVisibility(View.GONE);

                                    global.communication = new Communication(global, GameBoardActivity.this);

                                    gameView = new GameView(GameBoardActivity.this, global);
//                                    container.addView(gameView);
                                }
                            });
                        }
                    }
                });
        t.start();
    }

    private boolean isConnectedToNetwork() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo == null || !networkInfo.isConnected()) {
            Toast.makeText(this, global.getResources().getString(R.string.device_not_connected_network), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()
                            && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.i(TAG, "onBackPressed");
        try {
            Intent intent = new Intent(Constants.STOP_RUNNING_VIEW);
            sendBroadcast(intent);
            if (global.getPlayer().getPoints() == 0) {
                global.getPlayer().insertPointsOnSharedPreferences();
            }
            unregisterBroadcastReceiver();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Broadcast receiver that receives the details of the new game
    private void registerBroadcastReceiver() {

        receiver = new GameBoardBR();

        IntentFilter filter = new IntentFilter();

        filter.addAction(Constants.STOP_RUNNING_VIEW);
        registerReceiver(receiver, filter);

        filter.addAction(Constants.START_GAME);
        registerReceiver(receiver, filter);

        filter.addAction(Constants.USER_LOST_GAME);
        registerReceiver(receiver, filter);

        filter.addAction(Constants.USER_WON_GAME);
        registerReceiver(receiver, filter);

        filter.addAction(Constants.TRIGGER_NEXT_LEVEL);
        registerReceiver(receiver, filter);

        filter.addAction(Constants.TRIGGER_BONUS_FINISH);
        registerReceiver(receiver, filter);

        filter.addAction(Constants.OPPONENT_LOSE);
        registerReceiver(receiver, filter);

        filter.addAction(Constants.OPPONENT_WON);
        registerReceiver(receiver, filter);
    }

    private void unregisterBroadcastReceiver() {
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }

    public class GameBoardBR extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "Command received");

            switch (intent.getAction()) {
                case Constants.START_GAME:
                    Log.i(TAG, "BroadcastReceiver: StartGame");
                    pd.dismiss();
                    container.addView(gameView);

                    break;
                case Constants.USER_LOST_GAME:
                    Log.i(TAG, "BroadcastReceiver: User lose game");
                    global.sounds.playSound(3);
                    global.getPlayer().insertPointsOnSharedPreferences();
                    global.isGameRunning = false;

                    AlertDialog userLostDialog = new AlertDialog.Builder(context)
                            .setTitle(global.getResources().getString(R.string.you_lose))
                            .setMessage(global.getResources().getString(R.string.username) + global.getPlayer().getName() + "\n" +
                                    global.getResources().getString(R.string.points) + global.getPlayer().getPoints() + "\n" +
                                    global.getResources().getString(R.string.filled_area) + global.getPlayer().getFilledArea() + "\n" +
                                    global.getResources().getString(R.string.level) + global.getPlayer().getLevel())
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                    break;

                case Constants.USER_WON_GAME:
                    Log.i(TAG, "BroadcastReceiver: Usee won game");
                    global.sounds.playSound(2);
                    global.getPlayer().insertPointsOnSharedPreferences();
                    global.isGameRunning = false;

                    AlertDialog userWonDialog = new AlertDialog.Builder(context)
                            .setTitle(global.getResources().getString(R.string.congrats))
                            .setMessage(global.getResources().getString(R.string.username) + global.getPlayer().getName() + "\n" +
                                    global.getResources().getString(R.string.points) + global.getPlayer().getPoints() + "\n" +
                                    global.getResources().getString(R.string.filled_area) + global.getPlayer().getFilledArea() + "\n" +
                                    global.getResources().getString(R.string.level) + global.getPlayer().getLevel())
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    container.removeAllViews();
                                    global.resetGame();

                                    if (global.getPlayer().getLevel() != 7) {
                                        global.getPlayer().setLevel(global.getPlayer().getLevel() + 1);
                                        Intent intent = new Intent(Constants.TRIGGER_NEXT_LEVEL);
                                        sendBroadcast(intent);
                                    } else {
                                        finish();
                                    }
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();

                    break;

                case Constants.TRIGGER_NEXT_LEVEL:
                    Log.i(TAG, "BroadcastReceiver: Trigger next level");
                    switch (global.getPlayer().getLevel()) {
                        case 2:
                            gameView = new GameView(context, global, 5, Constants.BLACK, Constants.GREEN, Constants.BLUE);
                            container.addView(gameView);
                            break;
                        case 3:
                            gameView = new GameView(context, global, 6, Constants.RED, Constants.YELLOW, Constants.BLACK);
                            container.addView(gameView);
                            break;
                        case 4:
                            gameView = new GameView(context, global, 7, Constants.BLACK, Constants.GREEN, Constants.YELLOW);
                            container.addView(gameView);
                            break;
                        case 5:
                            gameView = new GameView(context, global, 8, Constants.RED, Constants.GREEN, Constants.BLACK);
                            container.addView(gameView);
                            break;
                        case 6:
                            gameView = new GameView(context, global, 9, Constants.YELLOW, Constants.GREEN, Constants.BLACK);
                            container.addView(gameView);
                            break;
                        case 7:
                            gameView = new GameView(context, global, 10, Constants.BLACK, Constants.GREEN, Constants.BLACK);
                            container.addView(gameView);
                            break;
                    }

                    break;
                case Constants.TRIGGER_BONUS_FINISH:
                    Log.i(TAG, "BroadcastReceiver: trigger bonus finish");
                    global.getBonusItems().clear();
                    break;

                case Constants.OPPONENT_LOSE:
                    Log.i(TAG, "BroadcastReceiver: OppontneLose");
                    global.sounds.playSound(2);
                    global.getPlayer().insertPointsOnSharedPreferences();
                    global.isGameRunning = false;

                    AlertDialog opponentLose = new AlertDialog.Builder(context)
                            .setTitle(global.getResources().getString(R.string.congrats))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    container.removeAllViews();
                                    global.resetGame();
                                    finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();

                    break;

                case Constants.OPPONENT_WON:
                    Log.i(TAG, "BroadcastReceiver: Opponent won the game");
                    global.sounds.playSound(3);
                    global.getPlayer().insertPointsOnSharedPreferences();
                    global.isGameRunning = false;

                    AlertDialog opponentWon = new AlertDialog.Builder(context)
                            .setTitle(global.getResources().getString(R.string.opponent_won))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    container.removeAllViews();
                                    global.resetGame();
                                    finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();

                    break;
            }
        }
    }
}
