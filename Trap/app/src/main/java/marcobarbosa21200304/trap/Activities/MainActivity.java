package marcobarbosa21200304.trap.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import marcobarbosa21200304.trap.Application.GlobalStateApplication;
import marcobarbosa21200304.trap.Player.LocalPlayer;
import marcobarbosa21200304.trap.R;
import marcobarbosa21200304.trap.util.Constants;

public class MainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    private GlobalStateApplication global;
    private SharedPreferences sharedpreferences;

    @Bind(R.id.tv_main_title)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        global = (GlobalStateApplication) getApplicationContext();
        sharedpreferences = getSharedPreferences(Constants.MY_PREFERENCES, MODE_PRIVATE);

        ButterKnife.bind(this);

        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/riverdrive.ttf");
        Typeface typeFace2 = Typeface.createFromAsset(getAssets(), "fonts/basictitlefont.ttf");

        tvTitle.setTypeface(typeFace);
    }

    @OnClick(R.id.bt_single_player)
    void startSinglePlayer() {
        global.resetAll();
        triggerDialog(false);
    }

    @OnClick(R.id.bt_multi_player)
    void startMultiPlayer() {
        global.resetAll();
        triggerDialog(true);
    }

    @OnClick(R.id.btn_best_scores)
    void startBestScoreActivity() {
        Intent intent = new Intent(this, BestScoreActivity.class);
        startActivity(intent);
    }

    private void triggerDialog(final boolean isMultiPlayer) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(global.getResources().getString(R.string.insert_username));

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LocalPlayer player = new LocalPlayer(false, 1, sharedpreferences);
                global.setPlayer(player);
                global.getPlayer().setName(input.getText().toString());

                Intent intent = new Intent(MainActivity.this, GameBoardActivity.class);
                intent.putExtra("isMultiPlayer", isMultiPlayer);
                startActivity(intent);
            }
        });

        builder.show();
    }
}
