package marcobarbosa21200304.trap.Communication;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;
import marcobarbosa21200304.trap.GameElements.Ball;
import marcobarbosa21200304.trap.util.Constants;
import marcobarbosa21200304.trap.util.ObjectFactory;

/**
 * Created by Marco on 9/10/2015.
 */
public class Communication {

    private final String TAG = this.getClass().getSimpleName();

    // General variables
    private Context context;
    private Handler handler = null;
    private GlobalStateApplication global;

    // Comunication variables
    private Socket socketGame;
    private ObjectInputStream clientInputStream;
    private ObjectOutputStream clientOutputStream;

    public Communication(GlobalStateApplication global, Context context) {

        this.global = global;
        this.socketGame = global.getPlayer().getSocket();
        this.context = context;
        this.handler = new Handler();
        commThread.start();
    }

    Thread commThread = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                clientOutputStream = new ObjectOutputStream(socketGame.getOutputStream());
                clientInputStream = new ObjectInputStream(socketGame.getInputStream());

                if (global.getPlayer().isServer()) {
                    Log.i(TAG, "Server: Sending initial information");
                    sendInitialInformationToClient();
                }

                while (!Thread.currentThread().isInterrupted()) {

                    final Message message = (Message) clientInputStream.readObject();
                    Log.i(TAG, "Message received: " + message.getCommand());

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (global.getPlayer().isServer()) {
                                clientMove(message);
                                sharedMoves(message);
                            } else {
                                serverMove(message);
                                sharedMoves(message);
                            }
                        }
                    });
                }
            } catch (Exception e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
//                        finish();
                        Toast.makeText(context, "O jogo foi finalizado", Toast.LENGTH_LONG).show();

                        // TODO: Show Dialog saying that i won
                    }
                });
            }
        }
    });

    private void clientMove(Message message) {

        switch (message.getCommand()) {

            case Constants.INITIAL_INFORMATION_COMMAND:

                Log.i(TAG, "[SERVER] Initial information received");
                if (message.getX() < global.screenSizeX) {
                    Log.i(TAG, "[SERVER] My screen is bigger. Adjusting");
                    global.getBox().set(0, 0, message.getScreenSizeX(), message.getScreenSizeY());
                    sendThatsFine();
                } else {
                    Log.i(TAG, "[SERVER] My screen is smaller");
                }

                Log.i(TAG, "[SERVER] Sending balls and start signal");
                sendBallsToClient();
                sendStartSignal();
                break;

            case Constants.THATS_FINE_COMMAND:

                Log.i(TAG, "[SERER] thats fine received");
                Log.i(TAG, "[SERVER] Sending balls and start signal");
                sendBallsToClient();
                sendStartSignal();

                break;

        }
    }

    private void serverMove(Message message) {

        switch (message.getCommand()) {

            // INITIAL COMMANDS
            case Constants.INITIAL_INFORMATION_COMMAND:
                if (message.getScreenSizeX() < global.screenSizeX) {
                    Log.i(TAG, "[CLIENT] My screen is bigger. Adjusting");
                    global.getBox().set(0, 0, message.getScreenSizeX(), message.getScreenSizeY());
                    sendThatsFine();

                } else {
                    Log.i(TAG, "[CLIENT] My Screen is smaller");
                    sendInitialInformationToClient();
                }
                break;

            case Constants.START_GAME_COMMAND:
                Log.i(TAG, "Start Game Command");
                Intent intent = new Intent(Constants.START_GAME);
                context.sendBroadcast(intent);
                break;
        }
    }

    private void sharedMoves(Message message) {

        switch (message.getCommand()) {
            case Constants.YOUR_TURN_COMMAND:

                //Updating opponent information
                global.getPlayerOpponent().setPoints(message.getPoints());
                global.getPlayerOpponent().setLives(message.getLives());
                global.getPlayerOpponent().setFilledArea(message.getFilledArea());

                //Updating personal information
                global.getPlayer().setIsMyTurn(true);
                break;

            case Constants.ADD_LINE_COMMAND:
                Log.i(TAG, "Add line command");
                ObjectFactory.createLine(global, message.getX(), message.getY(), message.isHorizontal());
                break;

            case Constants.ADD_BALL_COMMAND:
                Log.i(TAG, "Add ball command");
                ObjectFactory.createBall(global, message.getX(), message.getY(), message.getSpeedX(), message.getSpeedY(), message.getColor());
                break;

            case Constants.WIN_COMMAND:
                Log.i(TAG, "Add ball command");
                Intent intent = new Intent(Constants.OPPONENT_WON);
                global.sendBroadcast(intent);
                break;

            case Constants.LOSE_COMMAND:
                Log.i(TAG, "Add ball command");
                Intent intent1 = new Intent(Constants.OPPONENT_LOSE);
                global.sendBroadcast(intent1);
                break;
        }
    }

    public void sendItsYourTurn() {

        global.getPlayer().setIsMyTurn(false);
        Message turnMessage = new Message(Constants.YOUR_TURN_COMMAND);
        turnMessage.setPoints(global.getPlayer().getPoints());
        turnMessage.setLives(global.getPlayer().getLives());
        turnMessage.setFilledArea(global.getPlayer().getFilledArea());
        sendMessage(turnMessage);
    }

    public void sendLoose() {
        Message loseMessage = new Message(Constants.LOSE_COMMAND);
        loseMessage.setPoints(global.getPlayer().getPoints());
        sendMessage(loseMessage);
    }

    public void sendWin() {
        Message winMessage = new Message(Constants.WIN_COMMAND);
        winMessage.setPoints(global.getPlayer().getPoints());
        sendMessage(winMessage);
    }

    private void sendThatsFine() {
        Message thatsFine = new Message(Constants.THATS_FINE_COMMAND);
        sendMessage(thatsFine);
    }

    private void sendStartSignal() {

        Message startGame = new Message(Constants.START_GAME_COMMAND);
        sendMessage(startGame);

        Intent intent = new Intent(Constants.START_GAME);
        context.sendBroadcast(intent);
    }

    public void sendInitialInformationToClient() {
        Log.i(TAG, "Sending initial information");
        Message utilInfo = new Message(Constants.INITIAL_INFORMATION_COMMAND);
        utilInfo.setScreenSizeX(global.screenSizeX);
        utilInfo.setScreenSizeY(global.screenSizeY);

        sendMessage(utilInfo);
    }

    public void sendBallsToClient() {
        for (Ball ball : global.getBalls()) {
            sendBall(ball.ballX, ball.ballY, ball.speedX, ball.speedY, ball.getColor());
        }
    }

    public void sendBall(int x, int y, int speedX, int speedY, int color) {
        Message ball = new Message(global, Constants.ADD_BALL_COMMAND, x, y, speedX, speedY, color);
        sendMessage(ball);
    }

    public void sendLine(int x, int y, boolean isHorizontal) {
        Message line = new Message(global, Constants.ADD_LINE_COMMAND, x, y, isHorizontal);
        sendMessage(line);
    }

    private void sendMessage(Message message) {

        try {
            clientOutputStream.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
