package marcobarbosa21200304.trap.Communication;

import java.io.Serializable;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;

/**
 * Created by Marco on 9/11/2015.
 */
public class Message implements Serializable {

    // General Variables
    private static int idAux = 0;
    private int id;
    private int command;
    private int screenSizeX;
    private int screenSizeY;
    private int color;
    private int level;

    // Updating variables
    private int points;
    private int turn;
    private float filledArea;
    private int lives;

    // Ball Variables
    private int x;
    private int y;
    private int speedX;
    private int speedY;

    // Line Variables
    private boolean isHorizontal;

    public Message(int command) {
        this.command = command;
    }

    // Construtor for lines
    public Message(GlobalStateApplication global, int command, int x, int y, boolean isHorizontal) {
        this.command = command;
        this.x = x;
        this.y = y;
        this.isHorizontal = isHorizontal;
        this.points = global.getPlayer().getPoints();
    }

    // Construtor for balls
    public Message(GlobalStateApplication global, int command, int x, int y, int speedX, int speedY, int color) {
        this.command = command;
        this.x = x;
        this.y = y;
        this.speedX = speedX;
        this.speedY = speedY;
        this.color = color;
        this.points = global.getPlayer().getPoints();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isHorizontal() {
        return isHorizontal;
    }

    public void setIsHorizontal(boolean isHorizontal) {
        this.isHorizontal = isHorizontal;
    }

    public int getSpeedX() {
        return speedX;
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public int getSpeedY() {
        return speedY;
    }

    public void setSpeedY(int speedY) {
        this.speedY = speedY;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public float getFilledArea() {
        return filledArea;
    }

    public void setFilledArea(float filledArea) {
        this.filledArea = filledArea;
    }

    public int getScreenSizeX() {
        return screenSizeX;
    }

    public void setScreenSizeX(int screenSizeX) {
        this.screenSizeX = screenSizeX;
    }

    public int getScreenSizeY() {
        return screenSizeY;
    }

    public void setScreenSizeY(int screenSizeY) {
        this.screenSizeY = screenSizeY;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }
}
