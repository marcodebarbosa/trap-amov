package marcobarbosa21200304.trap.util;

/**
 * Created by Marco on 8/30/2015.
 */
public class Constants {

    // Colors
    public static final int BLACK = 0xff000000;
    public static final int BLUE = 0xff0000ff;
    public static final int RED = 0xffff0000;
    public static final int YELLOW = 0xffffff00;
    public static final int GREEN = 0xff00ff00;

    // Swipe values
    public static final int SWIPE_MIN_DISTANCE = 120;
    public static final int SWIPE_MAX_OFF_PATH = 250;
    public static final int SWIPE_THRESHOLD_VELOCITY = 200;
    public static final int LINE_STROKE = 12;

    // Connection
    public static final int PORT = 6000;
    public static final int PORTaux = 5000;

    // Communication commands
    public static final int ADD_LINE_COMMAND = 0;
    public static final int YOUR_TURN_COMMAND = 1;
    public static final int ADD_BALL_COMMAND = 2;
    public static final int START_GAME_COMMAND = 3;
    public static final int INITIAL_INFORMATION_COMMAND = 4;
    public static final int THATS_FINE_COMMAND = 5;
    public static final int WIN_COMMAND = 6;
    public static final int LOSE_COMMAND = 7;

    // Broadcast receivers
    public static final String STOP_RUNNING_VIEW = "marcobarbosa21200304.trap.util.stoprunning";
    public static final String START_GAME = "marcobarbosa21200304.trap.util.startgame";
    public static final String USER_LOST_GAME = "marcobarbosa21200304.trap.util.lostgame";
    public static final String USER_WON_GAME = "marcobarbosa21200304.trap.util.wongame";
    public static final String TRIGGER_NEXT_LEVEL = "marcobarbosa21200304.trap.util.nextlevel";
    public static final String TRIGGER_BONUS_FINISH = "marcobarbosa21200304.trap.util.finishbonus";
    public static final String OPPONENT_WON = "marcobarbosa21200304.trap.util.opponentwon";
    public static final String OPPONENT_LOSE = "marcobarbosa21200304.trap.util.opponentlose";

    // Others
    public static final String MY_PREFERENCES = "MyPrefs";
    public static final int BALL_RADIUS = 15;


}
