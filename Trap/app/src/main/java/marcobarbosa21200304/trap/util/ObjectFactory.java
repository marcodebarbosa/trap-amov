package marcobarbosa21200304.trap.util;

import android.graphics.Color;
import android.graphics.Rect;

import java.util.Random;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;
import marcobarbosa21200304.trap.BonusItems.BonusItem;
import marcobarbosa21200304.trap.BonusItems.FreezeBalls;
import marcobarbosa21200304.trap.BonusItems.LifeBonus;
import marcobarbosa21200304.trap.BonusItems.PointsBonus;
import marcobarbosa21200304.trap.GameElements.Area;
import marcobarbosa21200304.trap.GameElements.Ball;
import marcobarbosa21200304.trap.GameElements.Line;

/**
 * Created by Marco on 9/11/2015.
 */
public class ObjectFactory {

    public static void createLine(GlobalStateApplication global, int x, int y, boolean isHorizontal) {
        global.getLines().add(new Line(global, x, y, isHorizontal));
    }

    public static void createRandomBall(GlobalStateApplication global, int ballColor) {

        final int speedX = 1 + (int) (Math.random() * (20));
        final int speedY = 1 + (int) (Math.random() * (10));

        Random r = new Random();
        final int x = r.nextInt((global.screenSizeX - 100) - 1) + 1;
        final int y = r.nextInt((global.screenSizeY - 100) - 1) + 1;

        global.getBalls().add(new Ball(x, y, speedX, speedY, ballColor, global));
    }

    public static void createBall(GlobalStateApplication global, int x, int y, int speedX, int speedY, int color) {
        global.getBalls().add(new Ball(x, y, speedX, speedY, color, global));
    }

    public static void createArea(GlobalStateApplication global, Rect bounds, int color) {
        global.getAreas().add(new Area(bounds, Color.RED));
    }

    public static void createRandomBonusItem(GlobalStateApplication global) {

        final int speedX = 8;
        final int speedY = 3;
        final int whichBonus = 1 + (int) (Math.random() * (3));
        final int ballPositionToCreateBonus = 1 + (int) (Math.random() * (global.getBalls().size()));

        final int x = global.getBalls().get(ballPositionToCreateBonus - 1).ballX;
        final int y = global.getBalls().get(ballPositionToCreateBonus - 1).ballY;

        BonusItem object = null;
        switch (whichBonus) {
            case 1:
                object = new LifeBonus(x, y, speedX, speedY, Constants.YELLOW, global);
                break;
            case 2:
                object = new PointsBonus(x, y, speedX, speedY, Constants.BLUE, global);
                break;
            case 3:
                object = new FreezeBalls(x, y, speedX, speedY, Color.LTGRAY, global);
                break;
        }

        global.getBonusItems().add(object);
    }
}
