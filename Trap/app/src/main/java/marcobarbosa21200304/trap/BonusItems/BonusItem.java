package marcobarbosa21200304.trap.BonusItems;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;
import marcobarbosa21200304.trap.GameElements.Ball;
import marcobarbosa21200304.trap.GameElements.Box;
import marcobarbosa21200304.trap.GameElements.Line;
import marcobarbosa21200304.trap.util.Constants;

/**
 * Created by Marco on 9/13/2015.
 */
public class BonusItem extends Ball {

    private GlobalStateApplication global;

    public BonusItem(int ballX, int ballY, int speedX, int speedY, int color, GlobalStateApplication global) {
        super(ballX, ballY, speedX, speedY, color, global);
        this.global = global;
    }

    @Override
    public void onWallColision(Box box) {

        // Get new (x,y) position
        ballX += speedX;
        ballY += speedY;
        
        // Detect main collision and react
        if (ballX + Constants.BALL_RADIUS > box.xMax) {
            speedX = -speedX;
            ballX = box.xMax - Constants.BALL_RADIUS;

        } else if (ballX - Constants.BALL_RADIUS < box.xMin) {
            speedX = -speedX;
            ballX = box.xMin + Constants.BALL_RADIUS;
        }
        if (ballY + Constants.BALL_RADIUS > box.yMax) {
            speedY = -speedY;
            ballY = box.yMax - Constants.BALL_RADIUS;

        } else if (ballY - Constants.BALL_RADIUS < box.yMin) {
            speedY = -speedY;
            ballY = box.yMin + Constants.BALL_RADIUS;
        }
    }

    @Override
    public void onLineColision() {

        for (Line line : global.getLines()) {
            // Detect done line collision
            if (line.isInsideBounds(ballX + Constants.BALL_RADIUS, ballY)) {
                if (line.isDone()) {
                    speedX = -speedX;
                    ballX = line.xMin - Constants.BALL_RADIUS;
                    return;
                }

            } else if (line.isInsideBounds(ballX - Constants.BALL_RADIUS, ballY)) {
                if (line.isDone()) {
                    speedX = -speedX;
                    ballX = line.xMax + Constants.BALL_RADIUS;
                    return;
                }
            }
            if (line.isInsideBounds(ballX, ballY + Constants.BALL_RADIUS)) {
                if (line.isDone()) {
                    speedY = -speedY;
                    ballY = line.yMin - Constants.BALL_RADIUS;
                }
            } else if (line.isInsideBounds(ballX, ballY - Constants.BALL_RADIUS)) {

                if (line.isDone()) {
                    speedY = -speedY;
                    ballY = line.yMax + Constants.BALL_RADIUS;
                    return;
                }
            }
        }

    }

    public void superPower() {

    }

}
