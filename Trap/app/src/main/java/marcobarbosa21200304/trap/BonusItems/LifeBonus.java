package marcobarbosa21200304.trap.BonusItems;

import android.widget.Toast;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;

/**
 * Created by Marco on 9/13/2015.
 */
public class LifeBonus extends BonusItem {

    private GlobalStateApplication global;

    public LifeBonus(int ballX, int ballY, int speedX, int speedY, int color, GlobalStateApplication global) {
        super(ballX, ballY, speedX, speedY, color, global);
        this.global = global;
    }

    @Override
    public void superPower() {
        global.getPlayer().setLives(global.getPlayer().getLives() + 1);
        Toast.makeText(global, "Bonus: Life added!", Toast.LENGTH_SHORT).show();
    }
}
