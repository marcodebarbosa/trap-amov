package marcobarbosa21200304.trap.BonusItems;

import android.widget.Toast;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;

/**
 * Created by Marco on 9/13/2015.
 */
public class PointsBonus extends BonusItem {

    private GlobalStateApplication global;

    public PointsBonus(int ballX, int ballY, int speedX, int speedY, int color, GlobalStateApplication global) {
        super(ballX, ballY, speedX, speedY, color, global);

        this.global = global;
    }

    @Override
    public void superPower() {
        global.getPlayer().setPoints(global.getPlayer().getPoints() + 5000);
        Toast.makeText(global, "Bonus: +5000 points added! ", Toast.LENGTH_SHORT).show();
    }

}
