package marcobarbosa21200304.trap.Views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.widget.Toast;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;
import marcobarbosa21200304.trap.GameElements.Area;
import marcobarbosa21200304.trap.GameElements.Ball;
import marcobarbosa21200304.trap.GameElements.Box;
import marcobarbosa21200304.trap.GameElements.Line;
import marcobarbosa21200304.trap.GameElements.StatusMessage;
import marcobarbosa21200304.trap.R;
import marcobarbosa21200304.trap.util.Constants;
import marcobarbosa21200304.trap.util.ObjectFactory;

public class GameView extends SurfaceView {

    private final String TAG = this.getClass().getSimpleName();

    // General variables
    private GlobalStateApplication global;
    private GameViewBR receiver;
    private int bonusLuncher = 0;
    private int freezeCounter;

    // Screen variables
    private GestureDetector gestureScanner;
    private StatusMessage statusMsg;

    // FOR SINGLEPLAYER PURPOSES
    public GameView(Context context, GlobalStateApplication global, int numberBalls, int ballColor, int backgroundFirstColor, int backgroundSecondColor) {
        super(context);

        this.global = global;
        global.isGameRunning = true;
        gestureScanner = new GestureDetector(context, simpleOnGestureListener);
        global.setBox(new Box(backgroundFirstColor, backgroundSecondColor, GameView.this));

        for (int i = 0; i < numberBalls; i++) {
            ObjectFactory.createRandomBall(global, ballColor);
        }

        statusMsg = new StatusMessage(R.color.divider, context);
        setFocusableInTouchMode(true);
    }

    // FOR MULTIPLAYER PURPOSES
    public GameView(Context context, GlobalStateApplication global) {
        super(context);
        this.global = global;

        this.global.isGameMultiplayer = true;
        gestureScanner = new GestureDetector(context, simpleOnGestureListener);

        // Send message when this little box find out the numbers
        global.setBox(new Box(Constants.GREEN, Constants.BLACK, GameView.this));

        registerBroadcastReceiver();

        if (global.getPlayer().isServer()) {
            for (int i = 0; i < 4; i++) {
                ObjectFactory.createRandomBall(global, Constants.RED);
            }
        }

        statusMsg = new StatusMessage(R.color.divider, context);
        setFocusableInTouchMode(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (global.isGameRunning) {

            bonusLuncher++;
            if (bonusLuncher == 500) {
                ObjectFactory.createRandomBonusItem(global);
                bonusLuncher = 0;
            }
            if (global.isFreezeBalls()) {
                freezeCounter++;
                if (freezeCounter == 60) {
                    global.setFreezeBalls(false);
                }
            }
            global.getBox().draw(canvas);

            for (Area area : global.getAreas()) {
                area.draw(canvas);
            }

            for (Line line : global.getLines()) {
                line.draw(canvas);
            }

            for (Ball ball : global.getBalls()) {
                ball.draw(canvas);
                ball.onWallColision(global.getBox());
                ball.onLineColision();
            }

            for (Ball bonusItems : global.getBonusItems()) {
                bonusItems.draw(canvas);
                bonusItems.onWallColision(global.getBox());
                bonusItems.onLineColision();
            }

            statusMsg.draw(canvas);
            statusMsg.update();

            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            invalidate();  // Force a re-draw
        } else {
            if (global.isGameMultiplayer) {
                try {
                    unregisterBroadcastReceiver();
//                    TODO: NÃO PODE FICAR ASSIM : DA AQUI UMA EXCEPTION FATAL
                } catch (Exception e) {

                }
            }
        }
    }

    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        // Set the movement bounds for the ball
        Log.i(TAG, "OnSizeChanged");
        global.getBox().set(0, 0, w, h);
        global.calcTheArea();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureScanner.onTouchEvent(event);
    }

    public class GameViewBR extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Log.i(TAG, "Command received");
            switch (intent.getAction()) {
                case Constants.STOP_RUNNING_VIEW:
                    Log.i(TAG, "STOP_RUNNING_VIEW");
                    global.isGameRunning = false;
                    unregisterBroadcastReceiver();
                    break;
                case Constants.START_GAME:
                    global.isGameRunning = true;
                    invalidate();
                    break;
            }
        }
    }

    // Broadcast receiver that receives the details of the new game
    private void registerBroadcastReceiver() {

        receiver = new GameViewBR();

        IntentFilter filter = new IntentFilter();

        filter.addAction(Constants.STOP_RUNNING_VIEW);
        getContext().registerReceiver(receiver, filter);

        filter.addAction(Constants.START_GAME);
        getContext().registerReceiver(receiver, filter);
    }

    private void unregisterBroadcastReceiver() {
        if (receiver != null) {
            getContext().unregisterReceiver(receiver);
        }
    }

    GestureDetector.SimpleOnGestureListener simpleOnGestureListener = new GestureDetector.SimpleOnGestureListener() {

        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            if (!global.getPlayer().isMyTurn()) {
                Toast.makeText(getContext(), getContext().getResources().getString(R.string.not_your_turn), Toast.LENGTH_LONG).show();
            }
            // Allows just to accept touches from unfilled areas
            if (!global.isTouchingAreas((int) e1.getX(), (int) e1.getY()) && global.isGameRunning && global.getPlayer().isMyTurn()) {
                try {
                    if (Math.abs(e1.getY() - e2.getY()) > Constants.SWIPE_MAX_OFF_PATH && Math.abs(velocityY) > Constants.SWIPE_THRESHOLD_VELOCITY) {
//                        Log.i(TAG, "Down Swipe");
                        ObjectFactory.createLine(global, (int) e1.getX(), (int) e1.getY(), false);
                        if (global.isGameMultiplayer) {
                            global.communication.sendLine((int) e1.getX(), (int) e1.getY(), false);
                            global.communication.sendItsYourTurn();
                        }

                    } else if (Math.abs(e2.getY() - e1.getY()) > Constants.SWIPE_MAX_OFF_PATH && Math.abs(velocityY) > Constants.SWIPE_THRESHOLD_VELOCITY) {
//                        Log.i(TAG, "Up Swipe");
                        ObjectFactory.createLine(global, (int) e1.getX(), (int) e1.getY(), false);
                        if (global.isGameMultiplayer) {
                            global.communication.sendLine((int) e1.getX(), (int) e1.getY(), false);
                            global.communication.sendItsYourTurn();
                        }

                    } else if (e1.getX() - e2.getX() > Constants.SWIPE_MIN_DISTANCE && Math.abs(velocityX) > Constants.SWIPE_THRESHOLD_VELOCITY) {
//                        Log.i(TAG, "Left Swipe");
                        ObjectFactory.createLine(global, (int) e1.getX(), (int) e1.getY(), true);
                        if (global.isGameMultiplayer) {
                            global.communication.sendLine((int) e1.getX(), (int) e1.getY(), true);
                        }

                    } else if (e2.getX() - e1.getX() > Constants.SWIPE_MIN_DISTANCE && Math.abs(velocityX) > Constants.SWIPE_THRESHOLD_VELOCITY) {
//                        Log.i(TAG, "Right Swipe");
                        ObjectFactory.createLine(global, (int) e1.getX(), (int) e1.getY(), true);
                        if (global.isGameMultiplayer) {
                            global.communication.sendLine((int) e1.getX(), (int) e1.getY(), true);
                            global.communication.sendItsYourTurn();
                        }
                    }
                } catch (Exception e) {
                    Log.i(TAG, "Exception getting the swipe");
                    e.printStackTrace();
                    return false;
                }
            }
            return true;
        }
    };


}