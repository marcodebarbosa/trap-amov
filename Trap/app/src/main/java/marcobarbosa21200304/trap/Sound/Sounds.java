package marcobarbosa21200304.trap.Sound;

import android.media.MediaPlayer;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;
import marcobarbosa21200304.trap.R;

/**
 * Created by Marco on 9/13/2015.
 */
public class Sounds {

    private MediaPlayer breakingLineSound;
    private MediaPlayer winSound;
    private MediaPlayer looseSound;
    private MediaPlayer drawLineSound;
    private MediaPlayer filledAreaSound;
    private MediaPlayer bonusInsertedSound;
    private MediaPlayer speedLineSound;

    public Sounds(GlobalStateApplication global) {

        this.breakingLineSound = MediaPlayer.create(global, R.raw.break_line);
        this.winSound = MediaPlayer.create(global, R.raw.win);
        this.looseSound = MediaPlayer.create(global, R.raw.game_over);
        this.drawLineSound = MediaPlayer.create(global, R.raw.draw_line);
        this.filledAreaSound = MediaPlayer.create(global, R.raw.area_filled);
        this.bonusInsertedSound = MediaPlayer.create(global, R.raw.item_appear);
        this.speedLineSound = MediaPlayer.create(global, R.raw.speed_button);
    }

    public void playSound(int soundNumber) {

        switch (soundNumber) {
            case 1:
                breakingLineSound.start();
                break;
            case 2:
                winSound.start();
                break;
            case 3:
                looseSound.start();
                break;
            case 4:
                drawLineSound.start();
                break;
            case 5:
                filledAreaSound.start();
                break;
            case 6:
                bonusInsertedSound.start();
                break;
            case 7:
                speedLineSound.start();
                break;
        }
    }
}
