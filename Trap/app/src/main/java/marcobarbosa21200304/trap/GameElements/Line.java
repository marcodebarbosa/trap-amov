package marcobarbosa21200304.trap.GameElements;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;
import marcobarbosa21200304.trap.util.Constants;
import marcobarbosa21200304.trap.util.ObjectFactory;

/**
 * Created by Marco on 8/31/2015.
 */
public class Line {

    private final String TAG = this.getClass().getSimpleName();

    private GlobalStateApplication global;
    private Paint paint;
    private FillLine fillLine = new FillLine();
    private boolean isDone;
    public boolean isHorizontal;
    public int xMin;
    public int yMin;
    public int xMax;
    public int yMax;
    private Rect bounds;
    private int speed;

    public Line(GlobalStateApplication global, int pressedX, int pressedY, boolean isHorizontal) {

        this.global = global;
        this.bounds = new Rect();
        this.paint = new Paint();
        this.paint.setColor(Color.WHITE);
        this.paint.setShadowLayer(20, 0, 0, Color.BLACK);
        this.isHorizontal = isHorizontal;
        this.isDone = false;

        int MIN_VALUE = 0;
        int MAX_VALUE = 0;

        // Verification the left/right lines, to plan the limits of the line draw on horizontal cases
        if (isHorizontal) {
//            Log.i(TAG, "Horizontal");

            this.xMin = pressedX;
            this.xMax = pressedX;
            this.yMin = pressedY - Constants.LINE_STROKE;
            this.yMax = yMin + Constants.LINE_STROKE;

            MIN_VALUE = checkClosestVerticalLine(false);
            MAX_VALUE = checkClosestVerticalLine(true);

            fillLine.execute(MIN_VALUE, MAX_VALUE, pressedX, pressedY);

            // Verification the up/down lines, to plan the limits of the line draw on horizontal cases
        } else {
//            Log.i(TAG, "Vertical");

            this.yMin = pressedY;
            this.yMax = pressedY;
            this.xMin = pressedX - Constants.LINE_STROKE;
            this.xMax = xMin + Constants.LINE_STROKE;

            MIN_VALUE = checkClosestHorizontalLine(true);
            MAX_VALUE = checkClosestHorizontalLine(false);

            fillLine.execute(MIN_VALUE, MAX_VALUE, pressedX, pressedY);
        }

        global.sounds.playSound(4);
    }

    public void draw(Canvas canvas) {
        bounds.set(xMin, yMin, xMax, yMax);
        canvas.drawRect(bounds, paint);
    }

    private int checkClosestVerticalLine(boolean isRight) {

        ArrayList<Integer> linesAvaiable = new ArrayList<>();

        if (isRight) {
            // Check horizontal line below
            for (Line line : global.getLines()) {
                if (!line.isHorizontal) {
                    if (line.xMin > this.xMax) {
                        // Verify if they are in the same y value
                        if (this.yMin >= line.yMin && this.yMin <= line.yMax) {
                            linesAvaiable.add(line.xMin);
                        }
                    }
                }
            }

            if (!linesAvaiable.isEmpty()) {
                Collections.sort(linesAvaiable);
                return linesAvaiable.get(0);
            } else {
                return global.getBox().xMax;
            }

        } else {
            for (Line line : global.getLines()) {
                if (!line.isHorizontal) {
                    if (line.xMax < this.xMin) {
                        if (this.yMin >= line.yMin && this.yMin <= line.yMax) {
                            linesAvaiable.add(line.xMax);
                        }
                    }
                }
            }
            if (!linesAvaiable.isEmpty()) {
                Collections.sort(linesAvaiable, Collections.reverseOrder());
                return linesAvaiable.get(0);
            } else {
                return 0;
            }
        }
    }

    private int checkClosestHorizontalLine(boolean isUp) {

        ArrayList<Integer> linesAvaiable = new ArrayList<>();

        if (isUp) {
            // Check horizontal line below
            for (Line line : global.getLines()) {
                if (line.isHorizontal) {
                    if (line.yMax < this.yMin) {
                        if (this.xMin >= line.xMin && this.xMin <= line.xMax) {
                            linesAvaiable.add(line.yMax);
                        }
                    }
                }
            }

            if (!linesAvaiable.isEmpty()) {
                Collections.sort(linesAvaiable, Collections.reverseOrder());
                return linesAvaiable.get(0);
            } else {
                return 0;
            }

        } else {
            for (Line line : global.getLines()) {
                if (line.isHorizontal) {
                    if (line.yMin > this.yMax) {
                        if (this.xMin >= line.xMin && this.xMin <= line.xMax) {
                            linesAvaiable.add(line.yMin);
                        }
                    }
                }
            }
            if (!linesAvaiable.isEmpty()) {
                Collections.sort(linesAvaiable);
                return linesAvaiable.get(0);
            } else {
                return global.getBox().yMax;
            }
        }
    }

    public Rect canFillArea() {

        if (isHorizontal) {
            Rect areaUp = null;
            Rect areaDown = null;
            boolean hasAreaUp = false;
            final int Y_MIN;
            final int Y_MAX;

            Y_MIN = checkClosestHorizontalLine(false);

            //Limit is a wall below
            if (yMin == global.getBox().yMax) {
                areaUp = new Rect(xMin, yMin, xMax, global.getBox().yMax);
                if (!global.areaHasBalls(areaUp)) {
                    hasAreaUp = true;
                }
                // Limit is a line below
            } else {
                areaUp = new Rect(xMin, yMin, xMax, Y_MIN);
                if (!global.areaHasBalls(areaUp)) {
                    hasAreaUp = true;
                }
            }

            Y_MAX = checkClosestHorizontalLine(true);
            //Limit is the wall above
            if (Y_MAX == global.getBox().xMax) {
                //Limit is a wall
                areaDown = new Rect(xMin, 0, xMax, yMax);
                if (!global.areaHasBalls(areaDown)) {
                    if (hasAreaUp) {
                        return new Rect(areaUp.left, areaUp.top, areaUp.right, areaDown.bottom);
                    } else {
                        return areaDown;
                    }
                }
            } else {
                // Limit is a line above
                areaDown = new Rect(xMin, Y_MAX, xMax, yMax);
                if (!global.areaHasBalls(areaDown)) {
                    if (hasAreaUp) {
                        return new Rect(areaUp.left, areaUp.top, areaUp.right, areaDown.bottom);
                    } else {
                        return areaDown;
                    }
                }
            }
            if (hasAreaUp) {
                return areaUp;
            }
        } else {

            Rect areaRight;
            Rect areaLeft;
            boolean hasAreaRight = false;
            final int X_MIN;
            final int X_MAX;

            X_MIN = checkClosestVerticalLine(true);

            //Limit is the right wall
            if (X_MIN == global.getBox().xMax) {
                areaRight = new Rect(xMin, yMin, global.getBox().xMax, yMax);
                if (!global.areaHasBalls(areaRight)) {
                    hasAreaRight = true;
                }
                // Limit is a line
            } else {
                areaRight = new Rect(xMin, yMin, X_MIN, yMax);
                if (!global.areaHasBalls(areaRight)) {
                    hasAreaRight = true;
                }
            }

            // If the area to be checked is above
            X_MAX = checkClosestVerticalLine(false);

            //Limit is the left wall
            if (X_MAX == 0) {
                areaLeft = new Rect(0, yMin, xMax, global.getBox().yMax);
                if (!global.areaHasBalls(areaLeft)) {
                    if (hasAreaRight) {
                        new Rect(areaLeft.left, areaLeft.top, areaRight.right, areaLeft.bottom);
                    } else {
                        return areaLeft;
                    }

                }
            } else {
                // Limit is a line
                areaLeft = new Rect(X_MAX, yMin, xMax, yMax);
                if (!global.areaHasBalls(areaLeft)) {
                    if (hasAreaRight) {
                        new Rect(areaLeft.left, areaLeft.top, areaRight.right, areaLeft.bottom);
                    } else {
                        return areaLeft;
                    }
                }
            }
            if (hasAreaRight) {
                return areaRight;
            }
        }
        return null;
    }

    public boolean isInsideBounds(int x, int y) {
        return this.bounds.contains(x, y);
    }

    public boolean isDone() {
        return isDone;
    }

    public void cancelAsyncTask() {
        fillLine.stopAsync();
    }

    private class FillLine extends AsyncTask<Integer, Integer, Void> {

        private final String TAG = this.getClass().getSimpleName();
        private boolean isMinDrawing;
        private boolean isMaxDrawing;
        private boolean isRunning;
        private boolean isCancelled = false;
        private int i, j;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            Log.i(TAG, "Drawing Line");
            global.isDrawing = true;
            isRunning = true;
            isMinDrawing = true;
            isMaxDrawing = true;
//            drawLineSound.start();
        }

        @Override
        protected Void doInBackground(Integer... params) {

            int minValue = params[0];
            int maxValue = params[1];
            int pressedX = params[2];
            int pressedY = params[3];

            if (isHorizontal) {
                i = pressedX;
                j = pressedX;
            } else {
                i = pressedY;
                j = pressedY;
            }
            while (isRunning) {
                try {
                    if (i > minValue) {
                        i -= 15;
                    } else {
                        isMinDrawing = false;
                    }

                    if (j < maxValue) {
                        j += 15;
                    } else {
                        isMaxDrawing = false;
                    }

                    if (!isMinDrawing && !isMaxDrawing) {
                        isRunning = false;
                    } else {
                        publishProgress(i, j);
                        TimeUnit.NANOSECONDS.sleep(10);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            if (isHorizontal) {
                xMin = values[0];
                xMax = values[1];
            } else {
                yMin = values[0];
                yMax = values[1];
            }
//            Log.i(TAG, "Updating line value x: " + currentX + " and y: " + currentY);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            isDone = true;
            global.isDrawing = false;

            if (!isCancelled) {
                Rect areaBounds = canFillArea();

                if (areaBounds != null) {
                    global.sounds.playSound(5);
                    ObjectFactory.createArea(global, areaBounds, Color.RED);
                    global.areaHasBonus(areaBounds);
                    global.getPlayer().setPoints(global.getPlayer().getPoints() + (areaBounds.height() * areaBounds.width() / 100));

                    if (global.isGameMultiplayer && global.getPlayer().isMyTurn()) {
                        global.getPlayer().setFilledArea(global.getPlayer().getFilledArea() + global.getFilledPercentage(areaBounds));

                        if (global.getPlayer().getFilledArea() >= 75) {
                            global.communication.sendWin();
                            Intent intent = new Intent(Constants.USER_WON_GAME);
                            global.sendBroadcast(intent);
                        }

                    } else {
                        global.getPlayer().setFilledArea(global.getPlayer().getFilledArea() + global.getFilledPercentage(areaBounds));

                        if (global.getPlayer().getFilledArea() >= 75) {
                            Intent intent = new Intent(Constants.USER_WON_GAME);
                            global.sendBroadcast(intent);
                        }
                    }
                }
            }
        }

        private void stopAsync() {
//            Log.i(TAG, "Stop drawing Line");
            isRunning = false;
            isCancelled = true;
        }
    }
}
