package marcobarbosa21200304.trap.GameElements;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

import java.util.ArrayList;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;
import marcobarbosa21200304.trap.util.Constants;

public class Ball {

    private final String TAG = this.getClass().getSimpleName();

    private GlobalStateApplication global;
    protected int id;
    private static int idAux = 0;
    private int radius = Constants.BALL_RADIUS;
    public int ballX = Constants.BALL_RADIUS + 20;  // Ball2's center (x,y)
    public int ballY = Constants.BALL_RADIUS + 40;
    public int speedX = 25;
    public int speedY = 15;
    private int color;
    private RectF ballBounds;
    private Paint paint;
    protected boolean isRunning;

    public Ball(int ballX, int ballY, int speedX, int speedY, int color, GlobalStateApplication global) {

        this.global = global;
        paint = new Paint();
        paint.setColor(color);

        this.id = idAux;
        this.idAux++;
        this.color = color;
        this.ballX = ballX;
        this.ballY = ballY;
        this.speedX = speedX;
        this.speedY = speedY;

        ballBounds = new RectF();
    }

    public void onWallColision(Box box) {

        if (!global.isFreezeBalls()) {
            // Get new (x,y) position
            ballX += speedX;
            ballY += speedY;
        }

        // Detect main collision and react
        if (ballX + radius > box.xMax) {
            speedX = -speedX;
            ballX = box.xMax - radius;

        } else if (ballX - radius < box.xMin) {
            speedX = -speedX;
            ballX = box.xMin + radius;
        }
        if (ballY + radius > box.yMax) {
            speedY = -speedY;
            ballY = box.yMax - radius;

        } else if (ballY - radius < box.yMin) {
            speedY = -speedY;
            ballY = box.yMin + radius;
        }
    }

    public void onLineColision() {

        for (Line line : global.getLines()) {
            // Detect done line collision
            if (line.isInsideBounds(ballX + radius, ballY)) {
                if (line.isDone()) {
                    speedX = -speedX;
                    ballX = line.xMin - radius;
                    return;
                } else if (global.isDrawing) {
                    removeLine(line);
                    return;
                }

            } else if (line.isInsideBounds(ballX - radius, ballY)) {
                if (line.isDone()) {
                    speedX = -speedX;
                    ballX = line.xMax + radius;
                    return;
                } else if (global.isDrawing) {
                    removeLine(line);
                    return;
                }
            }
            if (line.isInsideBounds(ballX, ballY + radius)) {
                if (line.isDone()) {
                    speedY = -speedY;
                    ballY = line.yMin - radius;
                } else if (global.isDrawing) {
                    removeLine(line);
                    return;
                }
            } else if (line.isInsideBounds(ballX, ballY - radius)) {

                if (line.isDone()) {
                    speedY = -speedY;
                    ballY = line.yMax + radius;
                    return;
                } else if (global.isDrawing) {
                    removeLine(line);
                    return;
                }
            }

            if (line.isInsideBounds(ballX, ballY)) {
                if (global.isDrawing) {
                    removeLine(line);
                    return;
                }
            }
        }
    }

    private void removeLine(Line line) {

        global.sounds.playSound(1);

        line.cancelAsyncTask();
        global.getLines().remove(line);
        global.getPlayer().setLives(global.getPlayer().getLives() - 1);

        // Trigger game lost
        if (global.getPlayer().getLives() == 0) {
            Log.i(TAG, "No more lives. Game over!");
            Intent intent = new Intent(Constants.USER_LOST_GAME);
            global.sendBroadcast(intent);

            if (global.isGameMultiplayer) {
                global.communication.sendLoose();
            }

        }
    }

    public void onBallColision(ArrayList<Ball> balls, int pos) {

        for (int i = 0; i < balls.size(); i++) {
            if (i != pos) {
                // Detect collision and react
                if (ballX + radius == balls.get(i).ballX - radius) {
                    speedX = -speedX;
                    ballX = balls.get(i).ballX - radius;

                } else if (ballX - radius == balls.get(i).ballX + radius) {
                    speedX = -speedX;
                    ballX = balls.get(i).ballX + radius;
                }
                if (ballY + radius == balls.get(i).ballY - radius) {
                    speedY = -speedY;
                    ballY = balls.get(i).ballY - radius;

                } else if (ballY - radius == balls.get(i).ballY + radius) {
                    speedY = -speedY;
                    ballY = balls.get(i).ballY + radius;
                }
            }
        }
    }

    public void draw(Canvas canvas) {
        ballBounds.set(ballX - radius, ballY - radius, ballX + radius, ballY + radius);
        canvas.drawOval(ballBounds, paint);
    }

    public void setBallSpeed(int speedX, int speedY) {
        this.speedX = speedX;
        this.speedY = speedY;
    }

    // Getters and Setters

    public RectF getBallBounds() {
        return ballBounds;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setIsRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
