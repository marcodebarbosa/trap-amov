package marcobarbosa21200304.trap.GameElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import marcobarbosa21200304.trap.Application.GlobalStateApplication;

public class StatusMessage {

    private StringBuilder tvPoints = new StringBuilder();
    private StringBuilder tvFilledArea = new StringBuilder();
    private StringBuilder tvLives = new StringBuilder();

    private Paint paint;
    private GlobalStateApplication global;
    private int tvPosition;

    public StatusMessage(int color, Context context) {

        paint = new Paint();
        paint.setTypeface(Typeface.MONOSPACE);
        paint.setShadowLayer(20, 0, 0, Color.BLACK);
        paint.setTextSize(48);
        paint.setColor(color);
        global = (GlobalStateApplication) context.getApplicationContext();

        tvPosition = global.screenSizeY / 3;

        tvPoints.append("Points: ");
        tvFilledArea.append("Filled Area: ");
        tvLives.append("Lives: ");
    }

    public void update() {
        tvPoints.delete(8, tvPoints.length());
        tvPoints.append(global.getPlayer().getPoints());

        tvFilledArea.delete(13, tvFilledArea.length());
        tvFilledArea.append(global.getPlayer().getFilledArea());

        tvLives.delete(7, tvLives.length());
        tvLives.append(global.getPlayer().getLives());
    }

    public void draw(Canvas canvas) {

        canvas.drawText(tvPoints.toString(), 10, 60, paint);
        canvas.drawText(tvFilledArea.toString(), tvPosition * 2, 60, paint);
        canvas.drawText(tvLives.toString(), tvPosition * 4, 60, paint);
    }
}
