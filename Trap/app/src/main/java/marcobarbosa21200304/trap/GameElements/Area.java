package marcobarbosa21200304.trap.GameElements;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Marco on 9/2/2015.
 */
public class Area {

    private final String TAG = this.getClass().getSimpleName();

    private Paint paint;
    private Rect bounds;

    public Area(Rect bounds, int color) {

        this.paint = new Paint();
        this.paint.setColor(color);
        this.bounds = bounds;
    }

    public boolean isInsideBounds(int x, int y) {
        return this.bounds.contains(x, y);
    }

    public void draw(Canvas canvas) {
        canvas.drawRect(bounds, paint);
    }
}
