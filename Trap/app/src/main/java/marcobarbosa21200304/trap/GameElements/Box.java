package marcobarbosa21200304.trap.GameElements;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import marcobarbosa21200304.trap.Views.GameView;

public class Box {

    private final String TAG = this.getClass().getSimpleName();

    public int xMin, xMax, yMin, yMax;
    private Paint paint;
    private Rect bounds;

    public Box(int firstColor, int secondColor, GameView gameView) {
        paint = new Paint();
        paint.setColor(0x00000000);
        bounds = new Rect();

        backgroundAnimation(firstColor, secondColor, gameView);
    }

    public void set(int x, int y, int width, int height) {
        xMin = x;
        xMax = x + width - 1;
        yMin = y;
        yMax = y + height - 1;
        bounds.set(xMin, yMin, xMax, yMax);
    }

    private void backgroundAnimation(int firstColor, int secondColor, GameView gameView) {

        ValueAnimator colorAnim = ObjectAnimator.ofInt(gameView, "backgroundColor", firstColor, secondColor);
        colorAnim.setDuration(3000);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(ValueAnimator.INFINITE);
        colorAnim.setRepeatMode(ValueAnimator.REVERSE);
        colorAnim.start();
    }

    public void draw(Canvas canvas) {

        canvas.drawRect(bounds, paint);

        // To add stroke
//        paint.setStyle(Paint.Style.STROKE);
//        paint.setColor(Constants.BLUE);
//        canvas.drawRect(bounds, paint);
    }

}
