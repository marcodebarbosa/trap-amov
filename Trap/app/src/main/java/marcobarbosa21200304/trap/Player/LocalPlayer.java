package marcobarbosa21200304.trap.Player;

import android.content.SharedPreferences;

import java.net.Socket;

/**
 * Created by Marco on 9/11/2015.
 */
public class LocalPlayer extends Player {

    private SharedPreferences sharedPreferences;
    private Socket socket;
    private boolean isMyTurn;

    public LocalPlayer(boolean isServer, int level, SharedPreferences sharedPreferences) {
        super(isServer, level);
        this.sharedPreferences = sharedPreferences;
        this.isMyTurn = true;
    }

    @Override
    public void setPoints(int points) {
        super.setPoints(points);
    }

    public void insertPointsOnSharedPreferences() {

        // Update when wins
        // Update when finishes

        if (!sharedPreferences.contains("playerName1")) {
            SharedPreferences.Editor editor = sharedPreferences.edit();

            if (getName().isEmpty()) {
                editor.putString("playerName1", "player");
            } else {
                editor.putString("playerName1", getName());
            }
            editor.putInt("playerBestScore1", getPoints());
            editor.apply();

        } else if (!sharedPreferences.contains("playerName2")) {

            SharedPreferences.Editor editor = sharedPreferences.edit();

            if (getName().isEmpty()) {
                editor.putString("playerName2", "player");
            } else {
                editor.putString("playerName2", getName());
            }
            editor.putInt("playerBestScore2", getPoints());
            editor.apply();

        } else if (!sharedPreferences.contains("playerName3")) {

            SharedPreferences.Editor editor = sharedPreferences.edit();

            if (getName().isEmpty()) {
                editor.putString("playerName3", "player");
            } else {
                editor.putString("playerName3", getName());
            }
            editor.putInt("playerBestScore3", getPoints());
            editor.apply();

        } else if (getPoints() > this.sharedPreferences.getInt("playerBestScore1", 0)) {
            sharedPreferences.edit().putString("playerName1", getName()).apply();
            sharedPreferences.edit().putInt("playerBestScore1", getPoints()).apply();

        } else if (getPoints() > this.sharedPreferences.getInt("playerBestScore2", 0)) {
            sharedPreferences.edit().putString("playerName2", getName()).apply();
            sharedPreferences.edit().putInt("playerBestScore2", getPoints()).apply();

        } else if (getPoints() > this.sharedPreferences.getInt("playerBestScore3", 0)) {
            sharedPreferences.edit().putString("playerName3", getName()).apply();
            sharedPreferences.edit().putInt("playerBestScore3", getPoints()).apply();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public boolean isMyTurn() {
        return isMyTurn;
    }

    public void setIsMyTurn(boolean isMyTurn) {
        this.isMyTurn = isMyTurn;
    }
}
