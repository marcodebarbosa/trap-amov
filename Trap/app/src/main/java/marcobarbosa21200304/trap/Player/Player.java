package marcobarbosa21200304.trap.Player;

/**
 * Created by Marco on 9/6/2015.
 */
public class Player {

    // Player Variables
    private int id;
    private static int idAux = 0;
    private String name;
    private boolean isServer;
    private int points;
    private int level;
    private int lives;
    private float filledArea;

    public Player(boolean isServer, int level) {
        this.id = idAux;
        this.idAux++;
        this.isServer = isServer;
        this.lives = 3;
        this.points = 0;
        this.level = level;
        this.filledArea = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isServer() {
        return isServer;
    }

    public void setIsServer(boolean isServer) {
        this.isServer = isServer;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public float getFilledArea() {
        return filledArea;
    }

    public void setFilledArea(float filledArea) {
        this.filledArea = filledArea;
    }

    public void reset() {

        points = 0;
        filledArea = 0;
    }
}
