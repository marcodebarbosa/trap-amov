package marcobarbosa21200304.trap.Application;

import android.app.Application;
import android.content.Context;
import android.graphics.Rect;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.util.ArrayList;

import marcobarbosa21200304.trap.BonusItems.BonusItem;
import marcobarbosa21200304.trap.Communication.Communication;
import marcobarbosa21200304.trap.GameElements.Area;
import marcobarbosa21200304.trap.GameElements.Ball;
import marcobarbosa21200304.trap.GameElements.Box;
import marcobarbosa21200304.trap.GameElements.Line;
import marcobarbosa21200304.trap.Player.LocalPlayer;
import marcobarbosa21200304.trap.Player.Player;
import marcobarbosa21200304.trap.Sound.Sounds;

/**
 * Created by Marco on 8/31/2015.
 */
public class GlobalStateApplication extends Application {

    private final String TAG = this.getClass().getSimpleName();

    public boolean isGameRunning = false;

    // General Variables
    public int screenSizeX;
    public int screenSizeY;
    public int screenArea;
    public boolean isDrawing;
    public boolean isGameMultiplayer;

    // Communication
    public Communication communication;
    private LocalPlayer player;
    private Player playerOpponent;

    // Game Variables
    private ArrayList<Ball> balls;
    private ArrayList<Line> lines;
    private ArrayList<Area> areas;
    private ArrayList<BonusItem> bonusItems;
    private Box box;
    public Sounds sounds;
    private boolean freezeBalls = false;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Application started");

        getScreenSizeValues();

        balls = new ArrayList<>();
        lines = new ArrayList<>();
        areas = new ArrayList<>();
        bonusItems = new ArrayList<>();
        sounds = new Sounds(this);
        box = null;
    }

    public void getScreenSizeValues() {

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        // The variables were changes, bcs of the landscape orientation of the GameBoardActivity Screen
        screenSizeY = display.getWidth();
        screenSizeX = display.getHeight();
    }

    public float getFilledPercentage(Rect bounds) {

        int boundsArea = bounds.width() * bounds.height();

        return boundsArea * 100 / screenArea;
    }

    public boolean isTouchingLine(int x, int y) {
        for (Line line : lines) {
            if (line.isInsideBounds(x, y)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTouchingAreas(int x, int y) {
        for (Area area : areas) {
            if (area.isInsideBounds(x, y)) {
                return true;
            }
        }
        return false;
    }

    public boolean areaHasBalls(Rect area) {

        for (Ball ball : balls) {
            if (area.contains(ball.ballX, ball.ballY)) {
                return true;
            }
        }
        return false;
    }

    public void areaHasBonus(Rect area) {

        try {
            for (BonusItem bonusItem : bonusItems) {
                if (area.contains(bonusItem.ballX, bonusItem.ballY)) {
                    bonusItem.superPower();
                    bonusItems.remove(bonusItem);
                }
            }
        } catch (Exception e) {

        }
    }

    public void resetAll() {

        player = null;
        playerOpponent = null;
        balls.clear();
        lines.clear();
        areas.clear();
        bonusItems.clear();
        box = null;
        isGameRunning = false;
        isGameMultiplayer = false;
    }

    public void resetGame() {

        balls.clear();
        lines.clear();
        areas.clear();
        bonusItems.clear();
        box = null;
        isGameRunning = false;
        isGameMultiplayer = false;

        player.reset();
    }

    public void calcTheArea() {
        this.screenArea = box.xMax * box.yMax;
    }

    // Getters ad Setters

    public ArrayList<Ball> getBalls() {
        return balls;
    }

    public void setBalls(ArrayList<Ball> balls) {
        this.balls = balls;
    }

    public ArrayList<Line> getLines() {
        return lines;
    }

    public void setLines(ArrayList<Line> lines) {
        this.lines = lines;
    }

    public ArrayList<Area> getAreas() {
        return areas;
    }

    public void setAreas(ArrayList<Area> areas) {
        this.areas = areas;
    }

    public LocalPlayer getPlayer() {
        return player;
    }

    public void setPlayer(LocalPlayer player) {
        this.player = player;
    }

    public void setPlayerOpponent(Player playerOpponent) {
        this.playerOpponent = playerOpponent;
    }

    public Player getPlayerOpponent() {
        return playerOpponent;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    public ArrayList<BonusItem> getBonusItems() {
        return bonusItems;
    }

    public void setBonusItems(ArrayList<BonusItem> bonusItems) {
        this.bonusItems = bonusItems;
    }

    public void deleteBonusSpecItem(int id) {

        for (int i = 0; i < bonusItems.size(); i++) {
            if (bonusItems.get(i).getId() == id) {
                bonusItems.remove(i);
            }
        }
    }

    public boolean isFreezeBalls() {
        return freezeBalls;
    }

    public void setFreezeBalls(boolean freezeBalls) {
        this.freezeBalls = freezeBalls;
    }
}
